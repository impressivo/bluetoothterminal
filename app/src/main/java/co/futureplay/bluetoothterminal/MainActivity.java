package co.futureplay.bluetoothterminal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity {
    private ArrayAdapter<ListViewItem> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initDeviceListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        arrayAdapter.clear();
        for (BluetoothDevice device : BluetoothAdapter.getDefaultAdapter().getBondedDevices()) {
            arrayAdapter.add(new ListViewItem(device));
        }
    }

    private void initDeviceListView() {
        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_list_item);

        refresh();

        ListView deviceListView = (ListView) findViewById(R.id.DeviceListView);
        deviceListView.setAdapter(arrayAdapter);
        deviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewItem item = arrayAdapter.getItem(position);

                Intent intent = new Intent(MainActivity.this, ConnectionActivity.class);
                intent.putExtra("deviceAddress", item.getDevice().getAddress());
                startActivity(intent);
            }
        });
    }

    public static class ListViewItem {
        private final BluetoothDevice device;

        public ListViewItem(BluetoothDevice device) {
            this.device = device;
        }

        public String toString() {
            return device.getName() + "\n" + device.getAddress();
        }

        public BluetoothDevice getDevice() {
            return device;
        }
    }
}
