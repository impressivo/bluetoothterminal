package co.futureplay.bluetoothterminal;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ConnectionActivity extends ActionBarActivity {
    private static final UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String TAG = ConnectionActivity.class.getSimpleName();
    private static final int LINE_BREAK = 1000;

    private BluetoothSocket socket;
    private String address;

    private TextView statusText;
    private View statusLayout;
    private TextView console;
    private Switch hexModeSwitch;

    private OutputStream outputStream;
    private InputStream inputStream;
    private Thread readThread;

    private boolean hexMode = true;

    private State state = State.CONNECTING;
    private final List<Integer> bytesReceived = new ArrayList<>();
    private List<String> favorites = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        address = getIntent().getStringExtra("deviceAddress");
        statusText = (TextView) findViewById(R.id.status_text);
        hexModeSwitch = (Switch) findViewById(R.id.hex_switch);
        statusLayout = findViewById(R.id.status_layout);
        console = (TextView) findViewById(R.id.console);

        findViewById(R.id.send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

        findViewById(R.id.clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bytesReceived.clear();
                updateConsole();
            }
        });

        findViewById(R.id.break_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bytesReceived.add(LINE_BREAK);
                updateConsole();
            }
        });

        findViewById(R.id.reconnect_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
            }
        });

        findViewById(R.id.add_favorite_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFavorite();
            }
        });

        loadPreferences();

        hexModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hexMode = isChecked;
                updateConsole();
                savePreferences();
            }
        });

        connect();
    }

    @Override
    protected void onDestroy() {
        try {
            if (readThread != null) {
                readThread.interrupt();
            }
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "Failed to close the socket", e);
        }
        super.onDestroy();
    }

    private void connect() {
        state = State.CONNECTING;
        updateButtons();

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
                try {
                    socket = device.createRfcommSocketToServiceRecord(SERIAL_UUID);
                    socket.connect();
                    inputStream = socket.getInputStream();
                    outputStream = socket.getOutputStream();
                    readThread = new Thread(socketReader);
                    readThread.start();
                    state = State.CONNECTED;

                    return true;
                } catch (IOException e) {
                    Log.e(TAG, "Connection failure", e);
                    state = State.CONNECTION_FAILURE;
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                updateButtons();
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_connection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_disconnect) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadPreferences() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        hexMode = pref.getBoolean("hexMode", true);
        hexModeSwitch.setChecked(hexMode);

        String favoritesStr = pref.getString("favorites", null);
        favorites.clear();
        if (favoritesStr != null && !favoritesStr.isEmpty()) {
            for (String row : favoritesStr.split("\n")) {
                favorites.add(row);
            }
        }
        updateFavorites();
    }

    private void savePreferences() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = pref.edit();

        edit.putBoolean("hexMode", hexMode);

        String favoritesStr = "";
        for (int i = 0; i < favorites.size(); i++) {
            if (i > 0) {
                favoritesStr += "\n";
            }
            favoritesStr += favorites.get(i);
        }

        edit.putString("favorites", favoritesStr);
        edit.apply();
    }

    private void send() {
        EditText sendText = (EditText) findViewById(R.id.send_text);
        String s = sendText.getText().toString();

        try {
            outputStream.write(s.getBytes());
        } catch (IOException e) {
            e.printStackTrace(); // TODO
        }
    }

    private void updateButtons() {
        statusText.setText(state.toString());
        statusLayout.setVisibility(state == State.CONNECTED ? View.GONE : View.VISIBLE);
        findViewById(R.id.send_button).setEnabled(state == State.CONNECTED);
        findViewById(R.id.reconnect_button).setEnabled(state == State.CONNECTION_FAILURE || state == State.DISCONNECTED);
    }

    private void updateConsole() {
        StringBuilder s = new StringBuilder();
        synchronized (bytesReceived) {
            for (Integer i : bytesReceived) {
                if (i == LINE_BREAK) {
                    s.append('\n');
                } else if (hexMode) {
                    s.append(String.format("%02X ", i));
                } else {
                    s.append(Character.toChars(i));
                }
            }
        }
        console.setText(s.toString());
        console.post(new Runnable() {
            @Override
            public void run() {
                ((ScrollView) findViewById(R.id.console_scroll)).fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void updateFavorites() {
        final ViewGroup layout = (ViewGroup) findViewById(R.id.favorite_layout);
        while (layout.getChildCount() > 1) {
            layout.removeViewAt(0);
        }

        for (String row : favorites) {
            addFavoriteButton(row.charAt(0) == '1', row.substring(1));
        }
    }

    private void addFavoriteButton(final boolean hex, final String content) {
        final ViewGroup layout = (ViewGroup) findViewById(R.id.favorite_layout);

        final Button button = new Button(this);
        button.setText(content);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (state == State.CONNECTED) {
                    try {
                        if (hex) {
                            outputStream.write(hexStringToByteArray(content));
                        } else {
                            outputStream.write(content.getBytes());
                        }
                    } catch (IOException e) {
                        e.printStackTrace(); // TODO
                    }
                }
            }
        });

        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int index;
                for (int i = 0; i < layout.getChildCount(); i++) {
                    if (layout.getChildAt(i) == button) {
                        favorites.remove(layout.getChildCount() - i - 2);
                        updateFavorites();
                        savePreferences();
                        break;
                    }
                }
                return true;
            }
        });

        layout.addView(button, 0);
    }

    private void addFavorite() {
        @SuppressLint("InflateParams")
        final View dlgLayout = getLayoutInflater().inflate(R.layout.dialog_add_favorite, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add a favorite text to send");
        builder.setView(dlgLayout);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // will be overridden
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Switch hexSwitch = (Switch) dlgLayout.findViewById(R.id.hex_switch);
                EditText input = (EditText) dlgLayout.findViewById(R.id.content_text);
                String string = input.getText().toString();

                if (string.isEmpty()) {
                    Toast.makeText(ConnectionActivity.this, "Please enter the value", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (hexSwitch.isChecked()) {
                    try {
                        hexStringToByteArray(string);
                    } catch (IllegalArgumentException e) {
                        Toast.makeText(ConnectionActivity.this, "Invalid input. Example: \"6A7B\"", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                favorites.add((hexSwitch.isChecked() ? "1":"0") + string);
                savePreferences();
                updateFavorites();
                dialog.dismiss();
            }
        });
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();

        if (len % 2 != 0) {
            throw new IllegalArgumentException();
        }

        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            char c1 = s.charAt(i);
            char c2 = s.charAt(i + 1);

            if (!(c1 >= '0' && c1 <= '9' || c1 >= 'a' && c1 <= 'f' || c1 >= 'A' && c1 <= 'F')) {
                throw new IllegalArgumentException();
            }
            if (!(c2 >= '0' && c2 <= '9' || c2 >= 'a' && c2 <= 'f' || c2 >= 'A' && c2 <= 'F')) {
                throw new IllegalArgumentException();
            }

            data[i / 2] = (byte) ((Character.digit(c1, 16) << 4)
                    + Character.digit(c2, 16));
        }
        return data;
    }

    private Runnable socketReader = new Runnable() {
        @Override
        public void run() {
            byte[] buffer = new byte[2048];
            while (!Thread.interrupted()) {
                try {
                    final int read = inputStream.read(buffer);
                    synchronized (bytesReceived) {
                        for (int i = 0; i < read; i++) {
                            bytesReceived.add(0xFF & buffer[i]);
                        }
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateConsole();
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, "IO Error", e);

                    state = State.DISCONNECTED;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateButtons();
                        }
                    });

                    break;
                }
            }
        }
    };

    private static enum State {
        CONNECTING, CONNECTED, CONNECTION_FAILURE, DISCONNECTED
    }
}
